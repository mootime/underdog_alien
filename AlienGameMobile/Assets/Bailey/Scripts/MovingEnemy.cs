﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemy : MonoBehaviour
{

    private Vector3 startpos;
    private Vector3 endpos;

    [Tooltip("Click if car moves sideways (Reative to Game camera)")]
    public bool sideways = false;
    [Tooltip("How long it takes for the car to get to the other side")]
    public float speed = 5f;
    [Tooltip("How long the car waits between movements")]
    public float wait_time = 3f;
    [Tooltip("Offsets when the car starts moving using travel time. (speed + wait_time / 2) Useful if you have cars moving both up/down and across and want to easily offset them.")]
    public bool offset = false;
    [Tooltip("Don't touch. Used to adjust for different sized colliders")]
    public float move_trim = 0;

    //LeanTween.moveY(af, 540.0f, 1.0f).setEase(LeanTweenType.easeOutBounce);

    // Start is called before the first frame update
    void Start()
    {
    
        startpos = transform.position;

        if(sideways == false){
            endpos = startpos + (transform.InverseTransformDirection(Vector3.forward) * (15 - move_trim) );
        }
        else{
            endpos = startpos + (transform.InverseTransformDirection(Vector3.forward) * (-15 + move_trim) );
        }

        if(offset == false){
            moveto();
        }
        else{

            StartCoroutine(OffsetWait( (wait_time + speed)/2 ));

        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {


    }

    public void moveto(){


        LeanTween.move(gameObject, endpos, speed).setEase(LeanTweenType.easeOutQuad);
        StartCoroutine(Wait(wait_time + speed, "movefrom"));


    }

    public void movefrom(){

        LeanTween.move(gameObject, startpos, speed).setEase(LeanTweenType.easeOutQuad);
        StartCoroutine(Wait(wait_time + speed, "moveto"));

    }

    IEnumerator Wait(float t, string next){
        
        yield return new WaitForSeconds(t);

        if(next == "moveto"){

            moveto();

        }
        else{

            movefrom();

        }

        //Debug.Log("done");
        

    }//waitco

    IEnumerator OffsetWait(float t){

    yield return new WaitForSeconds(t);
    moveto();

    }

}//eof
