﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PC_PauseRestart : MonoBehaviour
{

    public GameMenuManager gmm;

    void Start(){

        gmm = GetComponent<GameMenuManager>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if((Input.GetKeyDown(KeyCode.R))){

          gmm.Retry();  

        }

        if((Input.GetKeyDown(KeyCode.Escape))){

          gmm.enable_canvas("PauseMenuCanvas");
          gmm.Disable_player(); 
          gmm.disable_canvas("PauseButtonCanvas"); 
          gmm.disable_canvas("RestartButtonCanvas");  

        }
        
    }
}
