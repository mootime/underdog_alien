﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{

    GameManager gamemanager;

    public bool canmove = true;
    public bool movedone = true;

    public int turnstaken = 0;

    private Vector2 start_touch_pos, end_touch_pos;

    public Text debugtxt;

    //anim and player model

    public GameObject modelfolder;
    public Animator playeranimator;

    // Start is called before the first frame update
    void Start()
    {

        gamemanager = GameObject.Find("/SystemsAndUI/GameManagerObject").GetComponent<GameManager>();
        modelfolder = GameObject.Find(gameObject.name + "/PlayerModel");
        playeranimator = GameObject.Find("PlayerModel/Alien").GetComponent<Animator>();
        Input.simulateMouseWithTouches = true;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
    
        //get some input

        if(Input.GetButton("AG_up")){
            MoveModel(0f);
            Move(Vector3.forward, 0.0f);

        }

         if(Input.GetButton("AG_down")){
            MoveModel(180f);
            Move(-Vector3.forward, 0.0f);

        } 

         if(Input.GetButton("AG_left")){
            MoveModel(-90f);
            Move(Vector3.left, 0.0f);

        } 

         if(Input.GetButton("AG_right")){
            MoveModel(90f);
            Move(Vector3.right, 0.0f);

        } 

        //mobile input

        //debug test (if in scene)

        if(GameObject.Find("/FPSCanvas") != null){

            GameObject.Find("/FPSCanvas/Text").GetComponent<Text>().text = ( Input.touchCount.ToString() + " | FPS: " + (1 / Time.unscaledDeltaTime).ToString("f0") + " | ");

        }

        //did the screen get pressed?
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began){
            start_touch_pos = Input.GetTouch(0).position;
        }

        //did that finger top touching and how far did it go?
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended){
                end_touch_pos = Input.GetTouch(0).position;

            if( Mathf.Abs((end_touch_pos.x - start_touch_pos.x)) > Mathf.Abs((end_touch_pos.y - start_touch_pos.y)) ){

                if ((end_touch_pos.x < start_touch_pos.x)){
                    MoveModel(-90f);
                    Move(Vector3.left, 0.0f);
                }

                if ((end_touch_pos.x > start_touch_pos.x)){
                    MoveModel(90f);
                    Move(Vector3.right, 0.0f);
                }

            }
            else{

                if ((end_touch_pos.y > start_touch_pos.y)){
                    MoveModel(0f);
                    Move(Vector3.forward, 0.0f);
                }

                if ((end_touch_pos.y < start_touch_pos.y)){
                    MoveModel(180f);
                    Move(-Vector3.forward, 0.0f);
                }

            }

            }

            //

    }//update

    //move function
    public void Move (Vector3 direction, float offset){

        if(canmove == true && movedone == true){

            movedone = false;

            RaycastHit[] hits;

            //lmao wtf why does unity not order the array?
            hits = Physics.RaycastAll(transform.position, transform.TransformDirection(direction), 32);

            //and if it has nothing to order it breaks lol
            if(hits.Length != 1){

                hits = hits.OrderBy(h => h.distance).ToArray();

            }

            //Debug.DrawRay(transform.position, transform.TransformDirection(direction), Color.yellow);

            //Debug.Log(hit.distance);

            //do hit checks
            for(int i = 0; i < hits.Length; i++){

                RaycastHit hit = hits[i];

                //Debug.Log(hits[0].transform.name + "," + hits[1].transform.name);

                if(hit.distance > 1f){

                    AudioFX.FXOneShot("launch_3" , "GameSoundSystem");
                    turnstaken ++;
                    
                }

                if(hit.transform.tag == "AttackBasic"){

                    Debug.Log("Attack on path");

                    RaycastHit secondhit = hits[i + 1];
                    //check and see if the second object we hit is the same as the first if so kill player
                if(secondhit.transform.tag == "enemyabduct" && secondhit.transform.gameObject == hit.transform.parent.gameObject){
                        StartCoroutine (VisualMove (transform.position, transform.position + direction * (hit.distance + 0.5f), false , hit.transform.gameObject));
                        break;
                    }
                    else{
                        //set it to check else where
                        hit = hits[i + 1];

                    }

                }

                if(hit.transform.tag == "FireClip"){

                    Debug.Log("FireClip on path");
                    //set it to check else where
                    hit = hits[i + 1];

                    

                }

                if(hit.transform.tag == "HazardBasic"){

                    StartCoroutine (VisualMove (transform.position, transform.position + direction * (hit.distance + 0.5f), false , null));
                    break;

                }

                if(hit.transform.tag == "abduct" || hit.transform.tag == "enemyabduct"){

                    //transform.position = transform.position + (direction * (hit.distance - 0.5f) );
                    StartCoroutine (VisualMove (transform.position, transform.position + direction * (hit.distance - 0.5f), true, hit.transform.gameObject ));
                    //hit.transform.gameObject.GetComponent<abduct>().dead();   
                    break;

                }

                if(hit.distance > 1.0f && hit.transform.tag == "Untagged"){

                    //transform.position = transform.position + (direction * (hit.distance - 0.5f) );
                    StartCoroutine (VisualMove (transform.position, transform.position + direction * (hit.distance - 0.5f), false , null));
                    break;

                }
                else{
                    //failsafe in case something is beyond boarder walls
                    movedone = true;
                    playeranimator.SetTrigger("ReturnIdle");
                    break;

                }

            }//for

        }

    }//move

    public void MoveModel (float mdlrot){

        if(movedone == true){
            modelfolder.transform.eulerAngles = new Vector3(0, mdlrot, 0);
        }

    }

    IEnumerator VisualMove (Vector3 startpos, Vector3 location, bool abduct, GameObject abductobj){

        playeranimator.SetTrigger("Run");

        float t = 0.0f;
        bool arrived = false;

        /*
        while(t < 1f){
            //Debug.Log(t);
            t += Time.deltaTime * 0.9f;
            //so we dont die when moving on accident
            transform.gameObject.tag = "PlayerMoving";
            //transform.position = Vector3.MoveTowards(transform.position, location, t);

                if(transform.position == location && arrived == false){
                    //making it so it's only called once!
                    arrived = true;
                    playeranimator.SetTrigger("ReturnIdle");

                    if(abduct == false){

                        gamemanager.finished_turn();
                        movedone = true;
                        //Debug.Log("done");

                    }
                    else{

                        //spawn abduct effect (probably using found enemy coordinates)
                        AudioFX.FXOneShot("beep_zap_fun_03" , "GameSoundSystem");
                        abductobj.GetComponent<abduct>().dead();
                        StartCoroutine(WaitCo(1.2f));

                    }

                    yield return null;

                }//arrive check

            yield return null;
        }//while
        */

        LeanTween.move(this.gameObject, location, 0.5f).setEase(LeanTweenType.easeInOutCirc);

        //so we dont die when moving on accident
        transform.gameObject.tag = "PlayerMoving";

        yield return new WaitForSeconds(0.5f);

        if(transform.position == location && arrived == false){
                //making it so it's only called once!
                arrived = true;
                playeranimator.SetTrigger("ReturnIdle");

                if(abduct == false){

                    gamemanager.finished_turn();
                    movedone = true;
                    //Debug.Log("done");

                }
                    else{

                        //spawn abduct effect (probably using found enemy coordinates)
                        AudioFX.FXOneShot("beep_zap_fun_03" , "GameSoundSystem");
                        abductobj.GetComponent<abduct>().dead();
                        StartCoroutine(WaitCo(1.2f));

                    }

                    yield return null;
            }
        yield return null;

        transform.gameObject.tag = "Player";


    }//visualMove

    IEnumerator WaitCo(float t){

        yield return new WaitForSeconds(t);
        gamemanager.finished_turn();
        movedone = true;
        //Debug.Log("done");

    }//waitco

}//eof
