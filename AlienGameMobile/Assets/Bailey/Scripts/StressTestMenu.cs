﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StressTestMenu : MonoBehaviour
{

    public Transform campivot;

    GameObject[] spawned;

    public string spawnloc = "Prefab/well_test_spawnable";
    int tm = 0;
    public Material flat;
    public Material texture; 
    public Material texturenormal; 

    public Text fpscount;
    public Text objcount;

    public float rot = 0;
    int multi = 0;

    int objects = 0;

    float fps = 0;
    float nextupdate = 0;
    float updaterate = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    /*if(Time.time > nextupdate){
	    nextupdate = Time.time + updaterate;
	    fps = 1/Time.deltaTime;
	}*/

    fps = 1 / Time.unscaledDeltaTime;

    fpscount.text = fps.ToString("f0");

    if(objcount != null){
        objcount.text = (objects.ToString() + " Aprox Tris " + (objects * 580) );
    }

    rot = rot + multi;

    if(campivot != null){
        campivot.rotation = Quaternion.Euler(0, rot, 0);
    }

    }

    public void More (){

         Instantiate(Resources.Load(spawnloc), new Vector3(Random.Range(-0.5f, 10.5f), 0, Random.Range(-0.5f, 10.5f)), Quaternion.Euler(-90, 0, 0));
         objects ++;

    }

    public void Less (){

        if(GameObject.FindWithTag("debugfinder") != null){
            Destroy(GameObject.FindWithTag("debugfinder"));
            objects --;
        }

    }

    public void TextureMode (){

        GameObject[] objs;

        tm ++;

        if(tm == 3){

            tm = 0;

        }
        
        objs = GameObject.FindGameObjectsWithTag("debugfinder");

        foreach (GameObject well_test_spawnable in objs){

            if(tm == 0){
                well_test_spawnable.GetComponent<Renderer>().material = flat;
            }
            if(tm == 1){
                well_test_spawnable.GetComponent<Renderer>().material = texture;
            }
            if(tm == 2){
                well_test_spawnable.GetComponent<Renderer>().material = texturenormal;
            }
        }
    
    }

    public void Right (){

        multi = -1;  

    }

    public void Left (){

        multi = 1;     

    }
}
