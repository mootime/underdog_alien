﻿//@ Bailey Schultz 2019 -Generic Music Player-

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{

    AudioSource muPlayer;
    public Object[] playlist;
    public int lastclip = 0;
    public int nextclip;
    public float muVol = 0.5f;


    // Start is called before the first frame update
    void Start()
    {

    muPlayer = GetComponent<AudioSource>();
    muPlayer.volume = muVol;
    playlist = Resources.LoadAll("Music", typeof(AudioClip));
    //set start track
    muPlayer.clip = playlist[Random.Range(0,playlist.Length)] as AudioClip;
            
    }

    // Update is called once per frame
    void Update()
    {

        if(!muPlayer.isPlaying){

            //prevent songs playing twice
            nextclip = Random.Range(0,playlist.Length);

            if(nextclip == lastclip){

                nextclip = Random.Range(0,playlist.Length);

            }

            lastclip = nextclip;
            muPlayer.clip = playlist[nextclip] as AudioClip;
            muPlayer.Play();


        }
              
    }

}
