﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cleaner : MonoBehaviour
{

public float destroytime = 5;
private float startime = 0;

    // Start is called before the first frame update
    void Start()
    {
      startime = Time.time;  
    }

    // Update is called once per frame
    void Update()
    {

        if(Time.time > (startime + destroytime)){

            Destroy(this.gameObject);

        }

    }
}
