﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayTimer : MonoBehaviour
{

    private TextMeshProUGUI timertext;
    GameManager gamemanager;

    private bool ended = false;

    [Tooltip("Time in seconds.")]
    public float time;



    // Start is called before the first frame update
    void Start()
    {

        gamemanager = GameObject.Find("/SystemsAndUI/GameManagerObject").GetComponent<GameManager>();
        timertext = GameObject.Find("/TimerCanvas/Text").GetComponent<TextMeshProUGUI>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(time <= 0 && ended == false){

            ended = true;

            //hacky way to force a game over
            //gamemanager.abducttargets = -1;

            //"kills" player
            GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().enable_canvas_bounce("GameOverCanvas");
            GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().Disable_player();
            GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().disable_ingamebuttons();


        }

        if(time > 0 && gamemanager.game_ended == false){

            time -= Time.fixedDeltaTime * 1;
            timertext.SetText( Mathf.RoundToInt(time).ToString() );

        }
        
    }
}
