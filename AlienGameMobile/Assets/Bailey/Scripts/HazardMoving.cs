﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMoving : MonoBehaviour
{
    public string hit_noise;
    public bool collided = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {



    }

    private void OnTriggerEnter(Collider collide) {
        
        if(collide.tag == "Player"|| collide.tag == "PlayerMoving"){

            if(collided == false){
                collided = true;
                //Pretty much anytime the player dies it's because of this!
                AudioFX.FXOneShot(hit_noise , "GameSoundSystem");
                GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().enable_canvas_bounce("GameOverCanvas");
                GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().Disable_player();
                GameObject.Find("/SystemsAndUI/GameOverCanvas").GetComponent<GameMenuManager>().disable_ingamebuttons();
                //GameObject.Find("/Player/PlayerModel/Alien").GetComponent<Animator>().SetTrigger("Death");IsDead
                GameObject.Find("/Player/PlayerModel/Alien").GetComponent<Animator>().SetBool("IsDead", true);
                Instantiate(Resources.Load("Prefab/SpawnBoom"), transform.position, transform.rotation);
                //Destroy(collide.gameObject);
            }
        }//main if
    }


}//eof

