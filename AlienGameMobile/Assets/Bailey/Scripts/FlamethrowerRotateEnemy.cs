﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FlamethrowerRotateEnemy : MonoBehaviour
{
    GameManager gamemanager;

    public bool clockwise = true;

    private int rot_dir = 90;

    public bool dev_override = false;
    public bool finished = false;

    public Animator animator;

    public  bool hasanim = false;

    public bool active = false;

    // Start is called before the first frame update
    void Start()
    {

        gamemanager = GameObject.Find("/SystemsAndUI/GameManagerObject").GetComponent<GameManager>();
        gamemanager.tb_objs ++;
        if(GameObject.Find(gameObject.name + "/Human").GetComponent<Animator>() != null){

            hasanim = true;
            animator = GameObject.Find(gameObject.name + "/Human").GetComponent<Animator>();

        }

        if(clockwise == false){

            rot_dir = -90;

        }

    }

    // Update is called once per frame
    void Update()
    {

    if(dev_override == true){

        gamemanager.turnbased_acm ++;
        dev_override = false;

    }

    if(gamemanager.gameturn == false){

        finished = false;
        active = false;

    }

    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.blue);

    if(gamemanager.gameturn == true && finished == false && active == false){

        active = true;

        //rotate
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + rot_dir, 0);

        //StartCoroutine(WaitRot(0.0f));
        shootflame();

    }

    }//update

    public void shootflame (){

            RaycastHit[] hits;
            AudioFX.FXOneShot("gas_large_flame_ignite_02" , "GameSoundSystem");

            if(hasanim == true){
            animator.SetTrigger("fire");
            }

            //lmao wtf why does unity not order the array?
            hits = Physics.RaycastAll(transform.position, transform.TransformDirection(Vector3.forward), 32);

            //and if it has nothing to order it breaks lol
            if(hits.Length != 1){

                hits = hits.OrderBy(h => h.distance).ToArray();

            }

            for(int i = 0; i < hits.Length; i++){

                RaycastHit hit = hits[i];

                if(hit.transform.tag == "AttackBasic" || hit.transform.tag == "HazardFlameThrower"){

                    Debug.Log("Attack on flame path");

                    RaycastHit secondhit = hits[i + 1];
                    hit = hits[i + 1];


                }

                if(hit.transform.tag == "Untagged" || hit.transform.tag == "HazardBasic" || hit.transform.tag == "FireClip"){


                    //for every 1m create fire (aka HazardBasic)
                    for(int f = 0; f < (hit.distance - 0.5f); f++){
                        //account for enemy pos and offset flames
                        Instantiate(Resources.Load("prefab/FlameThrower_HazardBasic"), (transform.position + transform.TransformDirection(Vector3.forward)) + (transform.TransformDirection(Vector3.forward) * f), transform.rotation);

                    }

                    StartCoroutine(WaitCo(1.2f));
                    finished = true;
                    break;

                }

                //players have the flame consume them while walls do not
                if(hit.transform.tag == "Player" || hit.transform.tag == "abduct" || hit.transform.tag == "enemyabduct"){

                    //for every 1m create fire (aka HazardBasic)
                    for(int f = 0; f < (hit.distance + 0.5f); f++){
                        //account for enemy pos and offset flames
                        Instantiate(Resources.Load("prefab/FlameThrower_HazardBasic"), (transform.position + transform.TransformDirection(Vector3.forward)) + (transform.TransformDirection(Vector3.forward) * f), transform.rotation);

                    }

                    StartCoroutine(WaitCo(1.2f));
                    finished = true;
                    break;

                }

            }//for

    }

    IEnumerator WaitCo(float t){

        yield return new WaitForSeconds(t);
        gamemanager.turnbased_acm ++;

    }

    IEnumerator WaitRot(float t){

        yield return new WaitForSeconds(t);
        shootflame();

    }
    

}//eof
