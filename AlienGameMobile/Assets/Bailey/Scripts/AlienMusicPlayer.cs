﻿//@ Bailey Schultz 2019 -Generic Music Player-

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlienMusicPlayer : MonoBehaviour
{

    AudioSource muPlayer;
    public Object[] playlist;
    public int lastclip = 0;
    public int nextclip;
    public float muVol = 0.5f;

    public string mu_subfolder = "Menu";
    public string currentlevel;
    public string chapter;


    // Start is called before the first frame update
    void Start()
    {

    muPlayer = GetComponent<AudioSource>();
    muPlayer.volume = muVol;
    playlist = Resources.LoadAll("Music/Menu", typeof(AudioClip));
    //set start track
    muPlayer.clip = playlist[Random.Range(0,playlist.Length)] as AudioClip;

    DontDestroyOnLoad(this.gameObject);
            
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        muPlayer.volume = PlayerPrefs.GetFloat("mu_vol", 1f);

        currentlevel = SceneManager.GetActiveScene().name;

        if(currentlevel.Contains("Menu")){

            mu_subfolder = "Menu";
            SwitchPlaylist(mu_subfolder);

        }
        
        if(currentlevel.Contains("farm")){

            mu_subfolder = "Farm";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("Area 51")){

            mu_subfolder = "Area51";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("C2")){

            mu_subfolder = "Suburbs";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("city")){

            mu_subfolder = "City";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("Desert")){

            mu_subfolder = "Desert";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("tutorial")){

            mu_subfolder = "Tutorial";
            SwitchPlaylist(mu_subfolder);

        }

        if(currentlevel.Contains("WH")){

            mu_subfolder = "Whitehouse";
            SwitchPlaylist(mu_subfolder);

        }

        if(!muPlayer.isPlaying){

            //prevent songs playing twice
            nextclip = Random.Range(0,playlist.Length);

            if(nextclip == lastclip){

                nextclip = Random.Range(0,playlist.Length);

            }

            lastclip = nextclip;
            muPlayer.clip = playlist[nextclip] as AudioClip;
            muPlayer.Play();


        }
              
    }

    public void SwitchPlaylist(string msf){

        playlist = Resources.LoadAll("Music/" + msf, typeof(AudioClip));
        //set start track
        muPlayer.clip = playlist[Random.Range(0,playlist.Length)] as AudioClip;

    }

}
