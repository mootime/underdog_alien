﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToOptions : MonoBehaviour
{

    public bool triggered = false;

    void OnSceneLoaded(Scene scene, LoadSceneMode mode){

        if(triggered == true){

            triggered = false;
            disable_canvas_all("MainMenuCanvas");
            enable_canvas_all("OptionsCanvas");

        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

      if(SceneManager.GetActiveScene().name != "MainMenu"){

          Destroy(this.gameObject);

      }

      SceneManager.sceneLoaded += OnSceneLoaded;

    }

    public void GoBack (){

        triggered = true; 

    }

    public void disable_canvas_all (string current_canvas){

         AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
         GameObject.Find("/" + current_canvas).GetComponent<Canvas>().enabled = false;

    }

    public void enable_canvas_all (string canvas_name){

        //Debug.Log(GameObject.Find(canvas_name));
        GameObject.Find("/" + canvas_name).GetComponent<Canvas>().enabled = true;

    }

}
