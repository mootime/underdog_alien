﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{

    private bool collided = false;
    Movement movement_script;


    // Start is called before the first frame update
    void Start()
    {

        movement_script = GameObject.Find("/Player").GetComponent<Movement>();
        
    }

    public void Disable_player(){

        if(collided == true){
            movement_script.enabled = true;
        }
        else{
            movement_script.enabled = false;
        }

    }

    public void OnTriggerStay(Collider collide) {
        
        if(collide.tag == "Player" && collided == false){

            Debug.Log("Tutorial Triggered");
           
            //AudioFX.FXOneShot(hit_noise , "GameSoundSystem");

            GameObject wc =  GameObject.Find(gameObject.name + "/TutorialCanvas");
            wc.GetComponent<Canvas>().enabled = true;

            Disable_player();

            collided = true;

        }

        if(collided == true){

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began){
                GameObject wc =  GameObject.Find(gameObject.name + "/TutorialCanvas");
                wc.GetComponent<Canvas>().enabled = false;

                StartCoroutine(WaitCo(0.6f)); 
            }

             if (Input.anyKey){
                GameObject wc =  GameObject.Find(gameObject.name + "/TutorialCanvas");
                wc.GetComponent<Canvas>().enabled = false;

                StartCoroutine(WaitCo(0.6f));
             }

        }
        
    }//trigger

    IEnumerator WaitCo(float t){

        yield return new WaitForSeconds(t);
        Disable_player();

    }

}
