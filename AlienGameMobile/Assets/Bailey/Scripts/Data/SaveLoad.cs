﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveLoad
{

    public static void Save (LevelData level, string lvlname){

        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/unity/ls/" + lvlname + ".dat", FileMode.Create);

        //LevelData data = new LevelData();

        bf.Serialize(stream, level);

        stream.Close();

    }

    public static LevelData Load (string lvlname){

      BinaryFormatter bf = new BinaryFormatter();
      FileStream stream = new FileStream(Application.persistentDataPath + "/unity/ls/" + lvlname + ".dat", FileMode.Open);

      LevelData data = bf.Deserialize(stream) as LevelData;
      stream.Close();

      return data;

    }

}
