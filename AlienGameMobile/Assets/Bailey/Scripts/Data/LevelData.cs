﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData
{

    public string levelname;
    //0 being locked, 1 being unlocked
    public int unlocked;
    //0 = none, 1 = 1 etc
    public int stars;

    //number of turned needed to get each level star
    public int one_star;
    public int two_star;
    public int three_star;
    public int turnstaken;

}
