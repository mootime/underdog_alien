﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class listdata
{
    public List<item> itemlist = new List<item>();
    public Dictionary<string, lvldat> levels = new Dictionary<string, lvldat>();


}

[System.Serializable]
public class item{

    public string itemname;
    public int amount;
    public int value;
    public string description;


}

[System.Serializable]
public class lvldat{

    public string itemname;
    public int amount;
    public int value;
    public string description;


}
