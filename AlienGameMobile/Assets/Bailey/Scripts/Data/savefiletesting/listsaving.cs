﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class listsaving
{

    public static void Saveitem (List<item> il)
    {

    FileStream fs = new FileStream(Application.persistentDataPath + "/test/item.dat", FileMode.Create);
    BinaryFormatter bf = new BinaryFormatter();
    bf.Serialize(fs, il);
    fs.Close();
    Debug.Log("Saved");

    }


    public static List<item> Loaditem()
    {

    FileStream fs = new FileStream(Application.persistentDataPath + "/test/item.dat", FileMode.Open);
    BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
    List<item> itemlist = (List<item>)bf.Deserialize(fs);
    fs.Close();
    Debug.Log("Loaded");
    return itemlist;

    }

    //---------------------------------------------------------------------------------------------------------------

    public static void Savedir (Dictionary<string, lvldat> il)
    {

    FileStream fs = new FileStream(Application.persistentDataPath + "/test/leveldir.dat", FileMode.Create);
    BinaryFormatter bf = new BinaryFormatter();
    bf.Serialize(fs, il);
    fs.Close();
    Debug.Log("Saved");

    }


    public static Dictionary<string, lvldat> Loaddir()
    {

    FileStream fs = new FileStream(Application.persistentDataPath + "/test/leveldir.dat", FileMode.Open);
    BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
    Dictionary<string, lvldat> levels = (Dictionary<string, lvldat>)bf.Deserialize(fs);
    fs.Close();
    Debug.Log("Loaded");
    return levels;

    }


}
