﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Level : MonoBehaviour
{
    public LevelData localdata;

    LevelData data;

    public GameObject LevelStarImage;

    public void Start() {

        localdata.levelname = gameObject.name;

        //Debug.Log("/" + transform.parent.name + "/" + gameObject.name + "/StarImage");

        if(GameObject.Find("/" + transform.parent.name + "/" + gameObject.name + "/StarImage") != null){

            LevelStarImage = GameObject.Find("/" + transform.parent.name + "/" + gameObject.name + "/StarImage");

        }

        if(File.Exists(Application.persistentDataPath + "/unity/ls/" + localdata.levelname + ".dat") || gameObject.name == "GameManagerObject"){

            //no need to rewrite (load instead) mainmenu buttons take from objects name levels do not
            LevelData data;
            
            if(gameObject.name != "GameManagerObject"){
                data = SaveLoad.Load(localdata.levelname);
            }
            else{
                data = SaveLoad.Load(SceneManager.GetActiveScene().name);
            }

            //force a reload of star requirement data so we can edit it in the future cuz all the values are the same but no one noticed and I just wanna be done with this 
            if(SceneManager.GetActiveScene().name == "MainMenu"){

                data.one_star = localdata.one_star;
                data.two_star = localdata.two_star;
                data.three_star = localdata.three_star;

            }

            localdata = data;

            //Debug.Log("Game Save Loaded");

        }

        else{
            //create file (First Time Playing)
            Directory.CreateDirectory(Application.persistentDataPath + "/unity/ls");
            localdata.levelname = gameObject.name;
            localdata.unlocked = 0;

            //so that we can actually play the game on first boot (comment out the if if you want all lvls unlocked)
            if(gameObject.name == "tutorial_level_1"){
                localdata.unlocked = 1;
            }

            localdata.stars = 0;
            SaveLoad.Save(localdata, localdata.levelname);
            Debug.Log("Game Save Created");

        }//file exists if/else end

        //when a level is loaded unlock it (unless its mainmenu)
        if(SceneManager.GetActiveScene().name != "MainMenu"){
            localdata.unlocked = 1;
            SaveLoad.Save(localdata, localdata.levelname);
        }

        //assign star value
        //check if locked
        if(LevelStarImage != null){

            if(localdata.unlocked == 0){

                ColorBlock cb = GetComponent<Button>().colors;
                cb.colorMultiplier = 5.0f;
                GetComponent<Button>().colors = cb;
                GetComponent<Button>().interactable = false;

            }

            switch (localdata.stars){

                case 1:
                LevelStarImage.GetComponent<Image>().sprite = Resources.Load("UI/star_1", typeof(Sprite)) as Sprite;
                break;

                case 2:
                LevelStarImage.GetComponent<Image>().sprite = Resources.Load("UI/star_2", typeof(Sprite)) as Sprite;
                break;

                case 3:
                LevelStarImage.GetComponent<Image>().sprite = Resources.Load("UI/star_3", typeof(Sprite)) as Sprite;
                break;

            }
        }

        //maybe add another level value to keep track of level number and then have it set the text to that so you dont have to do it
        
    }//start

    public void PlayLevel (){

            AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
            SceneManager.LoadScene(localdata.levelname);

    }

    public void StarCheck(){

        if(localdata.turnstaken <= localdata.three_star){

            localdata.stars = 3;

        }

        if(localdata.turnstaken <= localdata.two_star && localdata.turnstaken > localdata.three_star){

            localdata.stars = 2;

        }

        if(localdata.turnstaken <= localdata.one_star && localdata.turnstaken > localdata.two_star){

            localdata.stars = 1;

        }

        if(localdata.turnstaken > localdata.one_star){

            localdata.stars = 0;

        }

    }

    public void UnlockNext(){

        //find and unlock next level

        LevelData nextlvldata;

        //fuck unity, basically to get the name to return not null I have to get the scene path (like directory path) and clip off the start off it so I'm left with the name only
        int buildindex = SceneManager.GetActiveScene().buildIndex + 1;
        string nextlvlstring = SceneUtility.GetScenePathByBuildIndex(buildindex);
        nextlvlstring = nextlvlstring.Substring(0, nextlvlstring.Length - 6).Substring(nextlvlstring.LastIndexOf('/') + 1);

        nextlvldata = SaveLoad.Load( nextlvlstring );

        nextlvldata.unlocked = 1;

        SaveLoad.Save( nextlvldata,  nextlvlstring );  

    }

    public void SaveProxy(){

        SaveLoad.Save(localdata, localdata.levelname);

    }

}//eof
