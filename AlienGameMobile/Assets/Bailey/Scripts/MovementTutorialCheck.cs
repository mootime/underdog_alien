﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTutorialCheck : MonoBehaviour
{

    private Vector2 start_touch_pos, end_touch_pos;

    public bool movedup = false;
    public bool moveddown = false;
    public bool movedleft = false;
    public bool movedright = false;

    private bool spawned = false;

    public Vector3 SpawnLocation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //touch
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended){
                end_touch_pos = Input.GetTouch(0).position;

            if( Mathf.Abs((end_touch_pos.x - start_touch_pos.x)) > Mathf.Abs((end_touch_pos.y - start_touch_pos.y)) ){

                if ((end_touch_pos.x < start_touch_pos.x)){
                    movedleft = true;
                }

                if ((end_touch_pos.x > start_touch_pos.x)){
                    movedright = true;
                }

            }
            else{

                if ((end_touch_pos.y > start_touch_pos.y)){
                    movedup = true;
                }

                if ((end_touch_pos.y < start_touch_pos.y)){
                    moveddown = true;
                }

            }

            }

        //keyboard

        if(Input.GetButton("AG_up")){
            movedup = true;

        }

         if(Input.GetButton("AG_down")){
            moveddown = true;

        } 

         if(Input.GetButton("AG_left")){
            movedleft = true;

        } 

         if(Input.GetButton("AG_right")){
            movedright = true;

        }

        //check all

        if(moveddown == true && movedup == true && movedleft == true && movedright == true && spawned == false){

            spawned = true;
            GameObject.Find("/Movement Tutorial Check/abduct").transform.position = SpawnLocation;
            Instantiate(Resources.Load("Prefab/SpawnBoom"), SpawnLocation, transform.rotation);

        }
        
    }
}
