﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayerSpawner : MonoBehaviour
{

    public GameObject muplayer_object;
    public string clonename = "AlienMusicPlayer(Clone)";

    // Prevents multiple music players from spawning when we go back to the menu :)
    void Start()
    {

        if(GameObject.Find(clonename) != true){

            Instantiate(muplayer_object);

        }

    }

}
