﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioFX
{
    public static void FXOneShot(string audioclip, string audiosourceobj)
    {
        GameObject.Find("/SystemsAndUI/" + audiosourceobj).GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("sfx_vol", 1f);

        //plays one sound effect from the sfx folder (no need to enter /sfx to every input :))
        if(GameObject.Find("/SystemsAndUI/" + audiosourceobj) != null){
            GameObject.Find("/SystemsAndUI/" + audiosourceobj).GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("SFX/" + audioclip));
        }
    }

}
