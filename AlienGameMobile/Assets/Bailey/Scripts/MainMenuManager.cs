﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.Rendering;
using UnityEngine.Rendering.LWRP;

public class MainMenuManager : MonoBehaviour
{

    public LightweightRenderPipelineAsset gfx_low;
    public LightweightRenderPipelineAsset gfx_med;
    //mobile and pc have differnt assets so remember to switch them out for builds
    public LightweightRenderPipelineAsset gfx_high;

    // Start is called before the first frame update
    void Start()
    {

       //load save data (once!) for menu generation

       //set volume sliders (they use playerprefs and not my savesystem for ease of use)
       GameObject.Find("/OptionsCanvas/MusicVol/Slider").GetComponent<Slider>().normalizedValue = PlayerPrefs.GetFloat("mu_vol", 0.8f);
       GameObject.Find("/OptionsCanvas/SFXVol/Slider").GetComponent<Slider>().normalizedValue = PlayerPrefs.GetFloat("sfx_vol", 1f);
       //GameObject.Find("/OptionsCanvas/Dropdown").GetComponent<TMP_Dropdown>().value = PlayerPrefs.GetInt("gfx_level", 0);
       GameObject.Find("/OptionsCanvas/Dropdown").GetComponent<TMP_Dropdown>().SetValueWithoutNotify(PlayerPrefs.GetInt("gfx_level", 0));

       QualitySettings.SetQualityLevel(5);

       switch(PlayerPrefs.GetInt("gfx_level", 0)){
            
            case 0:
                GraphicsSettings.renderPipelineAsset = gfx_low;
            break;

            case 1:
                GraphicsSettings.renderPipelineAsset = gfx_med;
            break;

            case 2:
                GraphicsSettings.renderPipelineAsset = gfx_high;
            break;

        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

       //GameObject.Find("/OptionsCanvas/pipelineasset").GetComponent<TextMeshProUGUI>().SetText(GraphicsSettings.renderPipelineAsset.name);

    }

    public void Load_lvl(string level_name){

        AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
        SceneManager.LoadScene(level_name);   

    }

    public void enable_canvas (string canvas_name){

        //Debug.Log(GameObject.Find(canvas_name));
        GameObject.Find("/" + canvas_name).GetComponent<Canvas>().enabled = true;

    }

    public void disable_canvas (string current_canvas){

         AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
         GameObject.Find("/" + current_canvas).GetComponent<Canvas>().enabled = false;

    }

    public void Save_Game (){

        //SaveLoad.Save(); 

    }

    public void quitgame (){
    
        Application.Quit();
    
    }

    public void cleardata (){
        //Directory.Delete(Application.persistentDataPath + "/unity/ls");
        AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");

        var fi = Directory.GetFiles(Application.persistentDataPath + "/unity/ls");
 
        for (int i = 0; i < fi.Length; i++) {
            File.Delete(fi[i]);
        }

        SceneManager.LoadScene("MainMenu");

    }

    public void set_mu_volume (float vol){

        PlayerPrefs.SetFloat("mu_vol", vol);

    }

    public void set_sfx_volume (float vol){

        PlayerPrefs.SetFloat("sfx_vol", vol);

    }

    public void set_gfx_setting (int option){

        Debug.Log(option);
        

        switch(option){
            
            case 0:
                GraphicsSettings.renderPipelineAsset = gfx_low;
                PlayerPrefs.SetInt("gfx_level", 0);
            break;

            case 1:
                GraphicsSettings.renderPipelineAsset = gfx_med;
                PlayerPrefs.SetInt("gfx_level", 1);
            break;

            case 2:
                GraphicsSettings.renderPipelineAsset = gfx_high;
                PlayerPrefs.SetInt("gfx_level", 2);
            break;

        }

        GameObject.Find("/BackToOptions(Clone)").GetComponent<BackToOptions>().GoBack();

        Debug.Log(GraphicsSettings.renderPipelineAsset.name);

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }

}//eof
