﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int abducttargets = 0;
    public int turnbased_acm = 0;
    public int tb_objs = 0;
    public bool gameturn = false;

    public bool game_ended = false;

    Movement movement_script;
    Level level_script;

    // Start is called before the first frame update
    void Start()
    {
        
        Application.targetFrameRate = 60;
        abducttargets = GameObject.FindGameObjectsWithTag("abduct").Length;
        movement_script = GameObject.Find("/Player").GetComponent<Movement>();
        level_script = GetComponent<Level>();
        level_script.localdata.unlocked = 1;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // !!! lvl end !!!
        if(abducttargets <= 0 && game_ended == false){

            //so end game logic only runs once
            game_ended = true;

            GameObject.Find("/SystemsAndUI/LevelEndCanvas").GetComponent<GameMenuManager>().enable_canvas_bounce("LevelEndCanvas");
            GameObject.Find("/SystemsAndUI/LevelEndCanvas").GetComponent<GameMenuManager>().Disable_player();
            GameObject.Find("/SystemsAndUI/LevelEndCanvas").GetComponent<GameMenuManager>().disable_ingamebuttons();
            GameObject.Find("/SystemsAndUI/LevelEndCanvas").GetComponent<LevelEndKeyboard>().enabled = true;

            //if you beat your high score rewrite if not dont!
            if(movement_script.turnstaken < level_script.localdata.turnstaken || level_script.localdata.turnstaken == 0){
                level_script.localdata.turnstaken = movement_script.turnstaken;
            }
            level_script.StarCheck();
            level_script.SaveProxy();
            level_script.UnlockNext();

        }

        if(tb_objs == turnbased_acm){

            turnbased_acm = 0;
            gameturn = false;
            movement_script.canmove = true;

        }

        //screenshot (PC X2 Res)
        if(Input.GetKeyDown(KeyCode.F2)){
            ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/unity/" + Time.time.ToString("f3") + ".png", 2);
            
            Debug.Log("Captured ScreenShot " + Application.persistentDataPath + "/unity/" + Time.time.ToString("f3") + ".png");
        }

    }

    public void finished_turn (){

        //turnbased_acm = 0;
        gameturn = true;
        movement_script.canmove = false;

    }



}
