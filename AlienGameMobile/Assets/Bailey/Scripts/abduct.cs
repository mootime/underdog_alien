﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abduct : MonoBehaviour
{
    GameManager gamemanager;

    //[Tooltip("Specifies which folder to load human textures from (Located in Resources/Materials/this variable)")]
    //public string texture_subfolder = skinsoptions.Normal.ToString();

    public enum texture_subfolder {Normal, Area51, Whitehouse}
    public texture_subfolder folder_selected;
    private string skinfolderactual;

    [Tooltip("Does this enemy need to be abducted to finish? Usually not if enemy.")]
    public bool essential = true;
    [Tooltip("Does this enemy has a turnbased move?.")]
    public bool turnbased_enemy = false;

    public Animator animator;

    public  bool hasanim = false;

    // Start is called before the first frame update
    void Start()
    {

        skinfolderactual = folder_selected.ToString();

        gamemanager = GameObject.Find("/SystemsAndUI/GameManagerObject").GetComponent<GameManager>();

        if(GameObject.Find(gameObject.name + "/Human").GetComponent<Animator>() != null){

            hasanim = true;
            animator = GameObject.Find(gameObject.name + "/Human").GetComponent<Animator>();

        }

        //select random person material (does not support female mdls for now)
        //change range accordingly!
        GameObject.Find(gameObject.name + "/Human/body").GetComponent<Renderer>().material = Resources.Load("Materials/" + skinfolderactual + "/person_mat_" + Random.Range(1,5)) as Material;

        if(hasanim == true){
        InvokeRepeating("pickanim_num", 5, 5);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {



    }

    private void OnTriggerEnter(Collider collide) {
        

        if(collide.tag == "Player"){
        //playanimation/sfx
            Instantiate(Resources.Load("Prefab/SpawnBoom"), transform.position, transform.rotation);
            if(essential == true){
                gamemanager.abducttargets --;
            }
            Destroy(this.gameObject);
        }
    }

    public void dead() {
        //playanimation/sfx
        Instantiate(Resources.Load("Prefab/beam_abduct_effect"), transform.position, transform.rotation);
        if(animator != null){
            animator.SetTrigger("abduct");
        }

        //delete target
        //if(GameObject.Find(gameObject.name + "/Danger Zone Symbol") != null){

            //Destroy(GameObject.Find(gameObject.name + "/Danger Zone Symbol"));

        //}

        //delete unwanted stuff (symbols, attacks etc)

        foreach(Transform child in this.transform){

          if(child.name != "Human"){

              Destroy(child.gameObject);

          }  

        }

        LeanTween.moveY(this.gameObject , 20f, 4.0f).setEase(LeanTweenType.easeOutBounce).setDelay(0.4f);

        if(essential == true){
            gamemanager.abducttargets --;
        }
        if(turnbased_enemy == true){
            gamemanager.tb_objs --;
        }

        Debug.Log(gameObject);
        if(gameObject.GetComponent<FlamethrowerEnemy>() != null){
            GetComponent<FlamethrowerEnemy>().enabled = false;
        }
        GetComponent<Cleaner>().enabled = true;

        //Destroy(this.gameObject);
    }

    public void pickanim_num(){

        animator.SetInteger("idle_anim", Random.Range(1,2));

    }
}
