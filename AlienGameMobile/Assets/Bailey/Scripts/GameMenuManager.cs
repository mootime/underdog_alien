﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuManager : MonoBehaviour
{

    Movement movement_script;

    // Start is called before the first frame update
    void Start()
    {
        if(GameObject.Find("/Player") != null){
            movement_script = GameObject.Find("/Player").GetComponent<Movement>();
        }
    }

    public void Retry(){

        AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);   

    }

    public void Disable_player(){

        movement_script.enabled = !movement_script.enabled;

    }

    public void Next_lvl(){

        AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void Load_lvl(string level_name){ 

        //if(SceneManager.LoadScene(level_name) != null){
            AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
            Debug.Log(level_name);
            SceneManager.LoadScene(level_name);

        //}
        //else{

            //Debug.Log("No Level with that Name Found :(");

        //} 

    }

    public void enable_canvas (string canvas_name){

        GameObject wc =  GameObject.Find("/SystemsAndUI/" + canvas_name);
        //Debug.Log(GameObject.Find(canvas_name));
        wc.GetComponent<Canvas>().enabled = true;

    }

    public void enable_canvas_bounce (string canvas_name){

        GameObject wc =  GameObject.Find("/SystemsAndUI/" + canvas_name);
        GameObject af =  GameObject.Find(wc.name + "/AnimFolder");

        //Debug.Log(GameObject.Find(canvas_name));
        wc.GetComponent<Canvas>().enabled = true;

        //anim
        af.GetComponent<RectTransform>().localPosition = new Vector3(0, 500, 0);
        //tweener/ unity is off by 540 units
        LeanTween.moveY(af, 540.0f, 1.0f).setEase(LeanTweenType.easeOutBounce);

    }

    public void enable_canvas_fade (string canvas_name){

        GameObject wc =  GameObject.Find("/SystemsAndUI/" + canvas_name);
        GameObject af =  GameObject.Find(wc.name + "/AnimFolder");

        //Debug.Log(GameObject.Find(canvas_name));
        wc.GetComponent<Canvas>().enabled = true;

        //anim
        //af.GetComponent<RectTransform>(). set alpha to 0
        //tweener/ unity is off by 540 units
        //LeanTween.alpha(af, 1f, 1.0f).setEase(LeanTweenType.easeIn);

    }

    public void disable_canvas (string current_canvas){

         AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
         GameObject.Find("/SystemsAndUI/" + current_canvas).GetComponent<Canvas>().enabled = false;

    }

    public void disable_canvas_all (string current_canvas){

         AudioFX.FXOneShot("Button_scale_8" , "GameSoundSystem");
         GameObject.Find("/" + current_canvas).GetComponent<Canvas>().enabled = false;

    }

    public void enable_canvas_all (string canvas_name){

        //Debug.Log(GameObject.Find(canvas_name));
        GameObject.Find("/" + canvas_name).GetComponent<Canvas>().enabled = true;

    }

    public void disable_ingamebuttons (){

         GameObject.Find("/SystemsAndUI/RestartButtonCanvas").GetComponent<Canvas>().enabled = false;
         GameObject.Find("/SystemsAndUI/PauseButtonCanvas").GetComponent<Canvas>().enabled = false;

    }

    public void enable_ingamebuttons (){

         GameObject.Find("/SystemsAndUI/RestartButtonCanvas").GetComponent<Canvas>().enabled = true;
         GameObject.Find("/SystemsAndUI/PauseButtonCanvas").GetComponent<Canvas>().enabled = true;

    }

    public void quitgame (){
    
        Application.Quit();
    
    }

}
