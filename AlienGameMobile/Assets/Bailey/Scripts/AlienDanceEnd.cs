﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienDanceEnd : MonoBehaviour
{

    public GameObject beam;

    // Start is called before the first frame update
    void Start()
    {
       StartCoroutine(WaitCo(72.5f)); 
    }

    IEnumerator WaitCo(float t){

        yield return new WaitForSeconds(t);
        beam.SetActive(true);
        LeanTween.moveY(this.gameObject, 6f, 2f).setEase(LeanTweenType.easeInOutCirc);

    }
}
