﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAd : MonoBehaviour
{

    public string gameId = "3841841";
    public string placementId = "GamePlayBannerAd";

    public bool testmode = true;

    IEnumerator Start(){

        Advertisement.Initialize(gameId, testmode);

        while(!Advertisement.IsReady(placementId)){

            yield return null;

        }

        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(placementId);

    }

}
